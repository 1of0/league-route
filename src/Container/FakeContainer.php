<?php

namespace League\Route\Container;

use Interop\Container\ContainerInterface;

class FakeContainer implements ContainerInterface
{
	/**
	 * {@inheritdoc}
	 */
	public function get($id)
	{
		if (!$this->has($id))
		{
			throw new ClassNotFoundException("Class $id does not exist");
		}
		return new $id();
	}

	/**
	 * {@inheritdoc}
	 */
	public function has($id)
	{
		return is_string($id) && class_exists($id);
	}
}