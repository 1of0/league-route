<?php

namespace League\Route\Container;

use Exception;
use Interop\Container\Exception\NotFoundException;

class ClassNotFoundException extends Exception implements NotFoundException
{
}