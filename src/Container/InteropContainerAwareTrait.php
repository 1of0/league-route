<?php

namespace League\Route\Container;

use Interop\Container\ContainerInterface;

trait InteropContainerAwareTrait
{
	/**
	 * @var \Interop\Container\ContainerInterface
	 */
	protected $container;
	/**
	 * Set a container.
	 *
	 * @param  \Interop\Container\ContainerInterface $container
	 * @return $this
	 */
	public function setContainer(ContainerInterface $container)
	{
		$this->container = $container;
		return $this;
	}
	/**
	 * Get the container.
	 *
	 * @return \Interop\Container\ContainerInterface
	 */
	public function getContainer()
	{
		return $this->container;
	}
}