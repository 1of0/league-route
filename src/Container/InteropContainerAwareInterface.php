<?php

namespace League\Route\Container;

use Interop\Container\ContainerInterface;

interface InteropContainerAwareInterface
{
	/**
	 * Set a container.
	 *
	 * @param  \Interop\Container\ContainerInterface $container
	 * @return $this
	 */
	public function setContainer(ContainerInterface $container);

	/**
	 * Get the container.
	 *
	 * @return \Interop\Container\ContainerInterface
	 */
	public function getContainer();
}